# Pattern Lab Node - Grunt Edition With Compass

## Prérequis

Cette version de patternlab fonctionne avec [docker](https://www.docker.com/)

### Installation Docker

```
    #Construction de l'image
    docker build -t patternlab .
    #Lancement de l'image
    docker run -it --rm -p 3000:3000 -v $(pwd):/var/www patternlab bash
```

### Mise à jour/installation des dépendances

* Lancé la commande suivante

    ```
    npm install
    ```

Lancez `npm install` à partir du repertoire contenant le fichier `package.json` pour télécharger toutes les dépendances utiles à patternLab.

## "Getting Started"

### Lister toutes les commandes grunt valides

Pour lister toutes les commandes grunt valides tapez:

    grunt patternlab:help

### Génération des fichiers PatternLab

Pour générer les fichiers PatternLab tapez:

    grunt

### "Watcher" les fichiers patternLab et re-générer les fichiers à la volée

Pour observer les fichier patternLab, re-générer les fichiers à la volée et les rendre accessible via une serveur "BrowserSync",  tapez:

    grunt patternlab:serve

Le BrowserSync est accessible à l'adresse suivante [http://192.168.20.2:3000](http://192.168.20.2:3000) sur tous les navigateurs en local.

## Autres commandes

### Installer un "StarterKit"

Pour installer un StarterKit spécifique provenant de GitHub tapez:

    npm install [starterkit-vendor/starterkit-name]

    grunt patternlab:loadstarterkit --kit=[starterkit-name]

### Extraire les fichiers générés par PatternLab pour les livraisons clients

Pour extraire les fichiers générés par PatternLab afin de les livrer aux clients, tapez la commande:

    grunt

Puis copier-coller le dossier "public" qui à été généré à la racine du projet