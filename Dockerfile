#Image
FROM starefossen/ruby-node:2-5

MAINTAINER Valentin LACOUR <valentin.lacour@businessdecision.com>

RUN gem install compass

RUN npm install -g bower grunt-cli

WORKDIR /var/www